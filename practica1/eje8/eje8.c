#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define EXECL "-l"
#define EXECLP "-lp"
#define EXECV "-v"
#define EXECVP "-vp"
#define BIN_PATH "/bin/"

/**
 * Llame a un programa en dependencia del marcado.
 *
 * @param command Que programa debe ejecutarse.
 * @param flag Como llamar al programa.hh
 * @return El estado de salida del programa.
 */
int execute(char *command, char *flag) {
    char path[] = BIN_PATH;
    strcat(path, command);
    char* arg[] = {path, NULL};

    if (command == NULL || flag == NULL) {
        fprintf(stderr, "ERROR: no command or no flag.\n");
        return EXIT_FAILURE;
    } else if (strcmp(EXECL, flag) == 0) {
        // Call by path, command-line arguments as list.
        return execl(path, command, (char *) NULL);
    }else if (strcmp(EXECLP, flag) == 0) {
        // Call by filename, command-line arguments as list.
        return execlp(command, command, (char *) NULL);
    }else if (strcmp(EXECV, flag) == 0) {
        // Call by path, command-line arguments as array.
        return execv(arg[0], arg);
    }else if (strcmp(EXECVP, flag) == 0) {
        arg[0] = command;
        //Call by filename, command-line arguments as array.
        return execvp(arg[0], arg);
    }
    return EXIT_FAILURE;
}

/**
 * Llama a los programas desde los argumentos de la linea de comandos en secuencia.
 * Tipo de ejecucion dependiendo de la marcado.
 * Default path is /bin/
 *
 */
int main(int argc, char *argv[]) {
    /**
     * Contiene el pid del hijo.
     */
    pid_t pid;

    /**
     * Contiene el pid de lo hijos.
     */
    pid_t cpid[argc];

    /**
     * El estado de salida
     *
     */
    int status;

    /**
     * Una variable int para el bucle.
     */
    int i;

    for (i = 1; i < (argc-1); i++) {
        if ((cpid[i] = fork()) < 0) {
            printf("Error al emplear fork\n");
            return EXIT_FAILURE;
        } else if (cpid[i] == 0) {
            exit(execute(argv[i], argv[argc-1]));
        } else {
            pid = wait(&status);
            printf("PADRE %d HIJO %d terminado\n", getpid() ,pid);
        }
    }

    exit(EXIT_SUCCESS);
}
