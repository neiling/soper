#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define NUMHIJOS 4
#define BUFSIZE_STR 25

/**
 * Imprimi los resultados.
 */
void print_result(pid_t pid, char *num1_p, char *num2_p, char *op, float total) {
    printf("Datos enviados a traves de la tuberia por el proceso PID=%d."
                   " Operando 1: %s. Operando 2: %s. %s: %.2f\n", pid, num1_p, num2_p, op, total);
}

/**
 * Inicializa las pipes
 */
void pipe_initialize(int n, int m, int fd[n][m]) {
    int i;

    for (i = 0; i < n; i++) {
        if (pipe(fd[i]) == -1) {
            perror("Error creando la tuberia\n");
            exit(EXIT_FAILURE);
        }
    }
}

/**
 * El proceso padre crea cuatro hijos. Cada hijo calcula un resultado.
 */
int main(int argc, char *argv[]) {
    /**
     * Contiene el pid de lo hijos.
     */
    pid_t cpid[NUMHIJOS];

    /**
     * Una variable int para el bucle.
     */
    int i;

    /**
     * El estado de salida
     *
     */
    int status;

    /**
     * El pipe de parde
     */
    int fdp[2];

    /**
     * Los pipes del hijos
     */
    int fdh[NUMHIJOS][2];

    /**
     * El tamano de numeros.
     */
    int nums_tam, num1_tam, num2_tam;

    /**
     * La cadena de numeros.
     */
    char nums[BUFSIZE_STR];

    /**
     * Los numeros en separado como int.
     */
    char *num1, *num2;

    /**
     * El signo para separar.
     */
    char s[2] = ",";

    /**
     * Los numeros en separado como cadena
     */
    char num1_p[BUFSIZE_STR], num2_p[BUFSIZE_STR];

    /**
     * Resultado total
     */
    float total;

    pipe_initialize(NUMHIJOS, 2, fdh);

    if (pipe(fdp) == -1) {
        perror("Error creando la tuberia\n");
        exit(EXIT_FAILURE);
    }

    printf("Introduzca dos numeros separados por una coma: ");
    scanf("%s", nums);

    nums_tam = strlen(nums);

    for (i = 0; i < NUMHIJOS; i++) {
        if ((cpid[i] = fork()) < 0) {
            printf("Error al emplear fork\n");
            exit(EXIT_FAILURE);
        } else if (cpid[i] == 0) {
            // El hijo lee los numeros.
            close(fdp[1]);
            read(fdp[0], &nums_tam, sizeof(int));
            read(fdp[0], nums, nums_tam * sizeof(char));
            // Separado los numeros.
            num1 = strtok(nums, s);
            num2 = strtok(NULL, s);
            // Tamano de numeros
            num1_tam = strlen(num1);
            num2_tam = strlen(num2);
            // Calcular
            switch (i) {
                case 0 :
                    total = (atof(num1) + atof(num2));
                    break;
                case 1 :
                    total = (atof(num1) - atof(num2));
                    break;
                case 2 :
                    total = (atof(num1) * atof(num2));
                    break;
                case 3 :
                    total = (atof(num1) / atof(num2));
                    break;
            }
            // El hijo escribe el resultado vuelta a el padre.
            close(fdh[i][0]);
            write(fdh[i][1], &cpid[i], sizeof(int));
            write(fdh[i][1], &num1_tam, sizeof(int));
            write(fdh[i][1], num1, strlen(num1));
            write(fdh[i][1], &num2_tam, sizeof(int));
            write(fdh[i][1], num2, strlen(num2));
            write(fdh[i][1], &total, sizeof(float));
            exit(EXIT_SUCCESS);
        } else {
            // El padre lee el tamano de numeros y espera.
            close(fdp[0]);
            write(fdp[1], &nums_tam, sizeof(int));
            write(fdp[1], nums, strlen(nums));
            wait(&status);
            // El padre lee el resultado.
            close(fdh[i][1]);
            read(fdh[i][0], &cpid[i], sizeof(int));
            read(fdh[i][0], &num1_tam, sizeof(int));
            read(fdh[i][0], num1_p, num1_tam * sizeof(char));
            read(fdh[i][0], &num2_tam, sizeof(int));
            read(fdh[i][0], num2_p, num2_tam * sizeof(char));
            read(fdh[i][0], &total, sizeof(float));
            // Imprimi el resultado.
            switch (i) {
                case 0 :
                    print_result(cpid[i], num1_p, num2_p, "Suma", total);
                    break;
                case 1 :
                    print_result(cpid[i], num1_p, num2_p, "Resta", total);
                    break;
                case 2 :
                    print_result(cpid[i], num1_p, num2_p, "Multiplicacion", total);
                    break;
                case 3 :
                    print_result(cpid[i], num1_p, num2_p, "Division", total);
                    break;
            }
        }
    }
    exit(EXIT_SUCCESS);
}
