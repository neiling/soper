#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_PROC 3

/**
 * Un ejemplo para el metodo fork(). Para crear un proceso hijo.
 */
int main(int argc, char *argv[]) {
    /**
     * Save the pid of a child process.
     */
    pid_t pid;

    /**
     * Una variable int para el bucle.
     */
    int i;

    /**
     * Con getpid() obtiene el pid del proceso.
     * Con getpid() obtiene el pid del proceso partend.
     */
    for (i = 0; i < NUM_PROC; i++) {
        if ((pid = fork()) < 0) {
            printf("Error al emplear fork\n");
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            printf("HIJO %d, PID %d, PADRE PPID %d\n", i, getpid(), getppid());
        } else {
            printf("PADRE %d\n", getpid());
        }
    }

    exit(EXIT_SUCCESS);
}
