#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_PROC 3

/**
 * Un ejemplo para el metodo fork(). Para crear un proceso hijo.
 */
int main(int argc, char *argv[]) {
    /**
     * Contiene el pid del hijo.
     */
    pid_t pid;

    /**
     * El estado de salida
     * Valor de retniorno exacto con WEXITSTATUS(status).
     */
    int status;

    /**
    * Una variable int para el bucle.
    */
    int i;

    /**
    * pid_t wait(int *wstatus); Espera hasta el proceso hijo ha terminado.
    * break; El proceso padre sale la vuelta, ya que tiene un hijo.
    */
    for (i = 0; i < NUM_PROC; i++) {
        if ((pid = fork()) < 0) {
            printf("Error al emplear fork\n");
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            printf("HIJO %d, PID %d, PADRE PPID %d\n", i, getpid(), getppid());
        } else {
            printf("PADRE %d\n", getpid());
            wait(&status);
            break;
        }
    }
    exit(EXIT_SUCCESS);
}
