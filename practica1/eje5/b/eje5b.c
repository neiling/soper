#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_PROC 3

/**
 * El proceso padre crea trecs hijos y espera de todos hasta que terminaron.
 */
int main(int argc, char *argv[]) {
    /**
     * Contiene el pid del hijo.
     */
    pid_t pid;

    /**
     * Contiene el pid de lo hijos.
     */
    pid_t cpid[NUM_PROC];

    /**
     * El estado de salida
     * 
     */
    int status;

    /**
     * Una variable int para el bucle.
     */
    int i;

    /**
     *  Numero de hijos.
     */
    int count = NUM_PROC;

    for (i = 0; i < NUM_PROC; i++) {
        if ((cpid[i] = fork()) < 0) {
            printf("Error al emplear fork\n");
            exit(EXIT_FAILURE);
        } else if (cpid[i] == 0) {
            printf("HIJO %d, PID %d, PADRE PPID %d\n", i, getpid(), getppid());
            exit(EXIT_SUCCESS);
        } else {
            printf("PADRE %d\n", getpid());
        }
    }

    /**
     * El proceso padre llama wait() para cada hijo.
     * No hay ninguna diferencia que el hijo termino al principio.
     */
    while (count > 0) {
        pid = wait(&status);
        printf("HIJO %d terminado\n", pid);
        count--;
    }


    exit(EXIT_SUCCESS);
}
