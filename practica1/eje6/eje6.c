#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_PROC 3
#define BUFSIZE_STR 80

/**
 * El El proceso padre crea un hijo que introduzca un nombre.
 * Los procesos hijos no comparten recursos con los padres.
 * Por eso, el padre no tiene acento en las variables de hijo.
 */
int main(void) {
    /**
     * Memoria para una cadena de 80 caracteres.
     */
    char *str = (char*) malloc(BUFSIZE_STR*(sizeof(char)));

    /**
     * Contiene el pid del hijo.
     */
    pid_t pid;

    /**
     * El estado de salida
     * Valor de retorno exacto con WEXITSTATUS(status).
     */
    int status;

    if ((pid = fork()) < 0) {
        printf("Error al emplear fork\n");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        printf("Enter your Name: ");
        fgets(str, BUFSIZE_STR, stdin);
        printf("HIJO: Hola %s", str);
        exit(EXIT_SUCCESS);
    } else {
        wait(&status);
        printf("PADRE: Hola %s\n", str);
    }
    exit(EXIT_SUCCESS);
}
